* termux-notification: add ability to change status bar icon. Big thanks to @yurimataev!
* termux-notification-list: include when a notification was posted in the notification-list output (thanks to @schoentoon)
* termux-notification-list: properly handle BigTextStyle and InboxStyle (thanks to @schoentoon)
* termux-notification: fix notification api piority on Android version>=8 (thanks to @matyapiro31)
